﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class User
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime ModificationDate { get; set; }
        public int RoleId { get; set; } // tego pola nie używamy ono jest błędne
        public int Active { get; set; }

        public virtual ICollection<UserCar> UserCar { get; set; } //1:N jeden uzytkownik wiele samochodow
    }

    
}
