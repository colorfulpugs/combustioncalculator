﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class Tank
    {
        public int TankId{ get; set; }
        public decimal NumberOfKm { get; set; }
        public string DidYouMissTank { get; set; }
        public decimal PrizeForLiter { get; set; }
        public decimal QuantityOfFuel { get; set; }
        public int CurrencyId { get; set; }
        public DateTime AddDate { get; set; }
        public int UserCarId { get; set; }

        public virtual Currency Currency { get; set; }
        public virtual UserCar UserCar { get; set; }
    }
}
