﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class UserCar
    {
        public int UserCarId { get; set; }
        public int UserId { get; set; }
        public int CarsId { get; set; }
        public int YearOfCar { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public decimal NumberOfKm { get; set; }
        public string Photo { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime ModificationDate { get; set; }

        public virtual Cars Cars { get; set; } 
        public virtual User User { get; set; }
        public virtual ICollection<Raport> Raport { get; set; } //1:N jeden usercar moze miec wiele raportow
        public virtual ICollection<Tank> Tank { get; set; } //1:N jeden usercar moze miec wiele tankowan

    }

}
