﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class Settings
    {
        public int SettingsId { get; set; }
        public int UserId { get; set; }
        public int LanguageId { get; set; }
        public DateTime ModificationDate { get; set; }

        public virtual User User { get; set; }
        public virtual Language Language { get; set; }
    }
}
