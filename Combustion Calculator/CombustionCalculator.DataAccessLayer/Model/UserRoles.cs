﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class UserRoles
    {
        public int UserRolesId { get; set; }
        public int RolesId { get; set; }
        public int UserId { get; set; }

        public virtual Roles Roles { get; set; }
        public virtual User User { get; set; }
    }
}
