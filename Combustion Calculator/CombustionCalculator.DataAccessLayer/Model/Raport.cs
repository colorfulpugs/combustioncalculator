﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class Raport
    {
        public int RaportId { get; set; }
        public int UserCarId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FilePath { get; set; }
        public string GenerateDate { get; set; }

        public virtual UserCar UserCar { get; set; }

    }
}
