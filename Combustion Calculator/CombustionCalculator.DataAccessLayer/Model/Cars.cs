﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class Cars
    {
        public int CarsId { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime ModificationDate { get; set; }
        public int StartOfProductionDate { get; set; }
        public int EndOfProductionDate { get; set; }
        public string PhotoPath { get; set; }
        public double EngineCapacity { get; set; }

        public virtual ICollection<UserCar> UserCar { get; set; } //1:N jeden model samochodu moze miec wiele uzytkownikow samochodow
    }
}
