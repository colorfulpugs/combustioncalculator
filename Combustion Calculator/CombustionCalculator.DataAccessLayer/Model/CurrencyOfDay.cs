﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class CurrencyOfDay
    {
        public int CurrencyOfDayId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime DateOfCurrency { get; set; }
        public decimal ExchangeRate { get; set; }

        public virtual Currency Currency { get; set; }
    }
}
