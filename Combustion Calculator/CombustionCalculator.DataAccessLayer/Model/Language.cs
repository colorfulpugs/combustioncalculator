﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.Model
{
    public class Language
    {
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public DateTime AddDate { get; set; }
    }
}
