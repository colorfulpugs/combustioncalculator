﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System.Data.Entity.Migrations;


namespace CombustionCalculator.DataAccessLayer.Data
{
    public class InsertToDB
    {
        public static void SeedInitData(DB db)
        {
            var userList = new List<User>
            {
                 new User() {UserId=1, Login="Admin", Password="admin", Name = "Katarzyna", LastName="Dędys", DateOfBirth=DateTime.Now, AddDate=DateTime.Now, ModificationDate=DateTime.Now, RoleId=1, Active=1 },
            };
            List<User> userInDb = db.User.ToList();
            var result = userList.Where(a => !userInDb.Select(b => b.UserId).Contains(a.UserId)).ToList();
            foreach (var c in result)
            {
                db.User.AddOrUpdate(c);
            }
            db.SaveChanges();



            var rolesList = new List<Roles>
            {
                new Roles() {RolesId = 1, Name="admin" },
                new Roles() {RolesId = 2, Name="user" },
            };
            List<Roles> rolesInDb = db.Roles.ToList();
            var resultRoles = rolesList.Where(a => !rolesInDb.Select(b => b.RolesId).Contains(a.RolesId)).ToList();
            foreach (var r in resultRoles)
            {
                db.Roles.AddOrUpdate(r);
            }
            db.SaveChanges();



            var userRolesList = new List<UserRoles>
            {
                new UserRoles() {UserRolesId = 1, RolesId=1, UserId=1 },
            };
            List<UserRoles> userRolesInDb = db.UserRoles.ToList();
            var resultUserRoles = userRolesList.Where(a => !userRolesInDb.Select(b => b.RolesId).Contains(a.UserId) && !userRolesInDb.Select(b => b.RolesId).Contains(a.UserId)).ToList();
            foreach (var u in resultUserRoles)
            {
                db.UserRoles.AddOrUpdate(u);
            }
            db.SaveChanges();



            var languageList = new List<Language>
            {
                new Language() {LanguageId = 1, Name="polski", AddDate = DateTime.Now}
            };
            List<Language> languageInDb = db.Language.ToList();
            var resultLanguage = languageList.Where(a => !languageInDb.Select(b => b.LanguageId).Contains(a.LanguageId)).ToList();
            foreach (var l in resultLanguage)
            {
                db.Language.AddOrUpdate(l);
            }
            db.SaveChanges();



            var settingsList = new List<Settings>
            {
                new Settings() {SettingsId = 1, UserId = 1, LanguageId=1, ModificationDate=DateTime.Now}
            };
            List<Settings> settingsInDb = db.Settings.ToList();
            var resultSettings = settingsList.Where(a => !settingsInDb.Select(b => b.SettingsId).Contains(a.SettingsId)).ToList();
            foreach (var s in resultSettings)
            {
                db.Settings.AddOrUpdate(s);
            }
            db.SaveChanges();

            var currency = new Currency() { Name = "PLN" };
            List<Currency> currencyInDb = db.Currency.Where(k => k.Name == "PLN").ToList();
            if(currencyInDb.Count == 0)
            {         
                    db.Currency.AddOrUpdate(currency);
                    db.SaveChanges();  
            }

            var carsList = new List<Cars>
            {
                new Cars() {CarsId=1, Brand="Opel",Model="Astra", AddDate=DateTime.Now, ModificationDate=DateTime.Now, StartOfProductionDate=1999, EndOfProductionDate=2010,  PhotoPath="C:hjh", EngineCapacity=1.4},
                new Cars() {CarsId=2, Brand="Opel",Model="Corsa", AddDate=DateTime.Now, ModificationDate=DateTime.Now, StartOfProductionDate=1997, EndOfProductionDate=2005,  PhotoPath="C:hjh", EngineCapacity=1.2},
                new Cars() {CarsId=3, Brand="Fiat",Model="Punto", AddDate=DateTime.Now, ModificationDate=DateTime.Now, StartOfProductionDate=1996, EndOfProductionDate=2007,  PhotoPath="C:hjh", EngineCapacity=1.3},
                new Cars() {CarsId=4, Brand="Fiat",Model="Bravo", AddDate=DateTime.Now, ModificationDate=DateTime.Now, StartOfProductionDate=2003, EndOfProductionDate=2011,  PhotoPath="C:hjh", EngineCapacity=1.6},
                new Cars() {CarsId=5, Brand="Fiat",Model="Seicento", AddDate=DateTime.Now, ModificationDate=DateTime.Now, StartOfProductionDate=1995, EndOfProductionDate=2002,  PhotoPath="C:hjh", EngineCapacity=1.0}
            };
            List<Cars> carsInDb = db.Cars.ToList();
            var resultCars = carsList.Where(a => !carsInDb.Select(b => b.CarsId).Contains(a.CarsId)).ToList();
            foreach (var ca in resultCars)
            {
                db.Cars.AddOrUpdate(ca);
            }
            db.SaveChanges();

        }

    }
}
