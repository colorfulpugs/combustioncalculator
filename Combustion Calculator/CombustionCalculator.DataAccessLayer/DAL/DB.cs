﻿using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombustionCalculator.DataAccessLayer.DAL
{
    public class DB : DbContext
    {

        public DB() : base ("DBCalculator")
        {

        }
        public DbSet<Cars> Cars { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<CurrencyOfDay> CurrencyOfDay { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<Raport> Raport { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<Tank> Tank { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserCar> UserCar { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

  
}
