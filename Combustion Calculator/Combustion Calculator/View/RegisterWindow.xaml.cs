﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using CombustionCalculator.DataAccessLayer;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System.Data.Entity.Migrations;

namespace Combustion_Calculator.View
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {

        private DB db = new DB();
        public RegisterWindow()
        {
            InitializeComponent();
            ErrorMessage.Foreground = Brushes.Red;
        }

        private void DoRegister_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DoRegister_Click(object sender, RoutedEventArgs e)
        {
            string passwordCheck = PasswordName_Text.Password;
            string loginCheck = UserName_Text.Text;
            string nameCheck = Name_Text.Text;
            string lastnameCheck = LastName_Text.Text;

            if (CheckLogin(loginCheck, nameCheck, lastnameCheck))
            {
                MessageBox.Show("Taki użytkownik już istnieje w bazie");
                Name_Text.Clear();
                LastName_Text.Clear();
                UserName_Text.Clear();
                PasswordName_Text.Clear();
                PasswordName_Text1.Clear();
                Name_Text.Focus();
            }
            else
            {
 
                this.Walidation();
            }
            
        }
        public void Walidation()
        {
           
            if (Name_Text.Text.Length == 0)
            {
                  ErrorMessage.Content = "Wpisz imie";
                  Name_Text.Focus(); 
            }
            else if (LastName_Text.Text.Length == 0)
            {
                ErrorMessage.Content = "Wpisz nazwisko";
                LastName_Text.Focus();
            }
            else if (DateOfBirth_Text.Text.Length == 0)
            {
                ErrorMessage.Content = "Wpisz datę urodzenia";
                DateOfBirth_Text.Focus();
            }
            else if (UserName_Text.Text.Length == 0)
            {
                ErrorMessage.Content = "Wpisz nazwę użytkownika";
                UserName_Text.Focus();
            }
            else if (PasswordName_Text.Password.Length == 0)
            {
                ErrorMessage.Content = "Wpisz hasło";
                PasswordName_Text.Focus();
            }
            else if (PasswordName_Text1.Password.Length == 0)
            {
                ErrorMessage.Content = "Wpisz hasło powtórnie";
                PasswordName_Text1.Focus();
            }
            else if (PasswordName_Text.Password != PasswordName_Text1.Password)
            {
                ErrorMessage.Content = "Hasła muszą być takie same";
                PasswordName_Text1.Clear();
                PasswordName_Text1.Focus();
            }
            else
            {
               
                User user = new User { Login = UserName_Text.Text, Password = PasswordName_Text.Password, Name = Name_Text.Text, LastName = LastName_Text.Text, DateOfBirth = (DateTime)DateOfBirth_Text.SelectedDate, AddDate = DateTime.Now, ModificationDate = DateTime.Now, RoleId = 2, Active = 1 };
                db.User.Add(user);
                db.SaveChanges();

                var userFromTable = db.User.Where(k => k.Name == Name_Text.Text && k.Login == UserName_Text.Text && k.LastName == LastName_Text.Text && k.Password == PasswordName_Text.Password).FirstOrDefault();
                UserRoles userRoles = new UserRoles { RolesId = 2, UserId = user.UserId };
                db.UserRoles.Add(userRoles);
                db.SaveChanges();
                

                InitializeComponent();
                ErrorMessage.Content = "";
                MessageBox.Show("Zarejestrowano");

                LoginWindow win = new LoginWindow();
                win.Show();
                this.Close();
            }
        }

        public bool CheckLogin(string user1, string name1, string lastname1)
        {
            var userFromBase = db.User.Where(k => k.Name == name1 && k.Login == user1 && k.LastName == lastname1).FirstOrDefault();


            if (userFromBase != null)
            {
                return true;
            }
            return false;
        }
    }
}
