﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using CombustionCalculator.DataAccessLayer.Data;
using Combustion_Calculator.Helpers;

namespace Combustion_Calculator.View
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private DB db = new DB();
        public LoginWindow()
        {
            InitializeComponent();
            InsertToDB.SeedInitData(db);
            RateHelper.InsertCurrenciesToDb();
            UserName_Text.Focus();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {

                
                string user = UserName_Text.Text;
                string password = PasswordName_Text.Password;
               


                if (CheckNameAndPassword(user, password))
                {
                    MessageBox.Show("Zalogowano");

                    var iduser = db.User.Where(k => k.Login == user).FirstOrDefault();
                    MainWindow win = new MainWindow(iduser.UserId);
                    win.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Niepoprawna nazwa użytkownika lub hasło", "Błąd logowania");
                    return;
                }
            }
        public bool CheckNameAndPassword(string user1, string password1)
        {
                var userFromTable = db.User.Where(k => k.Password == password1 && k.Login == user1).FirstOrDefault();


            if (userFromTable != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Register_Button_Click(object sender, RoutedEventArgs e)
        {
     
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            RegisterWindow win = new RegisterWindow();
            win.Show();
            this.Close();
        }
    }
}
