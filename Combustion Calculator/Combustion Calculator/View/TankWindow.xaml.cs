﻿using Combustion_Calculator.Control;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Combustion_Calculator.View
{
    /// <summary>
    /// Interaction logic for TankAdd.xaml
    /// </summary>
    public partial class TankWindow : Window
    {
        int id;
        DB db = new DB();
        MainWindow main;
        List<UserCar> carsList;
        string item;
        

        public TankWindow(int idUser, MainWindow mainWindow, string text)
        {
            id = idUser;
            main = mainWindow;
            item = text;
            InitializeComponent();
            AddCarsToCombo();
            errorMessage.Foreground = Brushes.Red;
        }
        public void AddCarsToCombo()
        {
            carsList = db.UserCar.ToList();
            foreach (var x in carsList)
            {
                if (x.UserId == id)
                {
                    SelectCar.Items.Add(x.Name);
                    
                }
            }
        }

        private void PickCar_Click(object sender, RoutedEventArgs e)
        {
            if (SelectCar.SelectedItem != null)
            {
                string name = (string)SelectCar.SelectedItem;
                main.Show();
                switch (item)
                {
                    case "menuItemAddTank":
                        {
                            main.ContentHolder.Content = new TankAddControl(id, main, name);

                        }
                        break;
                    case "menuItemModificationTank":
                        {
                            main.ContentHolder.Content = new TankModificationControl(id, main, name);
                            
                        }
                        break;
                    default:
                    break;
                }
                this.Close();
            }
            else
            {
                errorMessage.Content = "Zaznacz samochód";

            }
        }
    }
}
