﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CombustionCalculator.DataAccessLayer;
using CombustionCalculator.DataAccessLayer.Model;
using CombustionCalculator.DataAccessLayer.DAL;
using Combustion_Calculator.Control;

namespace Combustion_Calculator.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int id;
        private DB db = new DB();
        
        public MainWindow(int iduser)
        {
            id = iduser;
            
            //Cars cars = new Cars { CarsId = 1, Brand = "Opel", Model = "Astra", AddDate = DateTime.Now, ModificationDate = DateTime.Now, StartOfProductionDate = 1999, EndOfProductionDate = 2005, PhotoPath = "G/GIT", EngineCapacity = 1.8 };
            //db.Cars.Add(cars);
            //User user = new User { UserId = 1, Login = "Kasia", Password = "kd", Name = "Katarzyna", LastName = "Dedys", DateOfBirth = DateTime.Now, AddDate = DateTime.Now, ModificationDate = DateTime.Now, RoleId = 1, Active = 1 };
            //db.User.Add(user);
            //db.SaveChanges();
            InitializeComponent();
            var nameLogin =  db.User.Where(k => k.UserId == id).FirstOrDefault();
            this.Title = "Witaj - " + nameLogin.Login;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            var user = db.User.Where(k => k.UserId == id).FirstOrDefault();
            var userRoles = db.UserRoles.Where(k => k.UserId == id).FirstOrDefault();
            MenuItem menuItem = (MenuItem)sender;
            var text = (string)menuItem.Name;
           
                switch (text)
                {
                    case "menuItemSettings":
                    if (userRoles.RolesId != 1)
                    {
                        this.ContentHolder.Content = new SettingsControl(id, this);
                    }
                    else
                    {
                        this.ContentHolder.Content = new AdminSettingsControl(id, this);
                    }
                   
                    break;
                    case "menuItemAdd":
                    {
                        this.ContentHolder.Content = new CarsAddControl(id, this);
                    }
                    break;
                    case "menuItemModification":
                    {
                        this.ContentHolder.Content = new CarModificationControl(id, this);
                    }
                    break;
                    case "menuItemAddTank":
                    {
                        TankWindow tankWin = new TankWindow(id, this, text);
                        tankWin.ShowDialog();  
                    }
                    break;
                    case "menuItemModificationTank":
                    {
                        TankWindow tankWin = new TankWindow(id, this, text);
                        tankWin.ShowDialog();
                    }
                    break;
                    default:
                    break;
                }
            

        }

    }
}
