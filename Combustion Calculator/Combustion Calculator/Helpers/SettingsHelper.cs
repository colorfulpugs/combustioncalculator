﻿using CombustionCalculator.DataAccessLayer.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Combustion_Calculator.Helpers
{
    class SettingsHelper
    {
        public static bool CheckPassword(string password, DB db, int id)
        {
            var passwordFromTable = db.User.Where(k => k.Password == password && k.UserId == id).FirstOrDefault();
            if (passwordFromTable != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckIfPasswordTheSame(string passwordNew, string passwordConfirmation)
        {
            if (passwordNew != passwordConfirmation)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static void ChangePassword(string passwordOld, string passwordNew, DB db, int id)
        {
            var user = db.User.Where(k => k.Password == passwordOld && k.UserId == id).FirstOrDefault();
            user.Password = passwordNew;
            db.User.AddOrUpdate(user);
            db.SaveChanges();

        }
    }
}
