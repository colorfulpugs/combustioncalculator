﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombustionCalculator.DataAccessLayer;
using CombustionCalculator.RateExchange;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;

namespace Combustion_Calculator.Helpers
{
    public class RateHelper
    {
        public static List<Currency> GetCurrency()
        {
            DB db = new DB();
            List<Currency> currencyList = db.Currency.ToList();
            return currencyList;
        }

        public static void InsertCurrenciesToDb()
        {
            DB db = new DB();
            var list = XmlParser.GetCurrencyListFromWeb(@"http://api.nbp.pl/api/exchangerates/tables/a/");
            var currencyList = GetCurrency();
            foreach (var c in list)
            {
                
                var matchingvalues = currencyList.Where(k => k.Name.Contains(c.name)).FirstOrDefault();
                if (matchingvalues == null)
                {
                    Currency currency = new Currency { Name = c.name, Country = "" };
                    db.Currency.Add(currency);
                    db.SaveChanges();
                }
            }
           
        }
    }
}
