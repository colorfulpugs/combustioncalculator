﻿#pragma checksum "..\..\..\Control\CarModification.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "59295FDEAAC5F6FE86C66F47CDE2FC94"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Combustion_Calculator.Control;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Combustion_Calculator.Control {
    
    
    /// <summary>
    /// CarModification
    /// </summary>
    public partial class CarModificationControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\..\Control\CarModification.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView Cars_ListView;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Control\CarModification.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Color_Text;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Control\CarModification.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox YearOfCar_Text;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Control\CarModification.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NumberOfKm_Text;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Control\CarModification.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteCar;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Control\CarModification.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveEditionCar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Combustion Calculator;component/control/carmodification.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Control\CarModification.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Cars_ListView = ((System.Windows.Controls.ListView)(target));
            
            #line 14 "..\..\..\Control\CarModification.xaml"
            this.Cars_ListView.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Edition_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Color_Text = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.YearOfCar_Text = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.NumberOfKm_Text = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.DeleteCar = ((System.Windows.Controls.Button)(target));
            
            #line 29 "..\..\..\Control\CarModification.xaml"
            this.DeleteCar.Click += new System.Windows.RoutedEventHandler(this.DeleteCar_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.SaveEditionCar = ((System.Windows.Controls.Button)(target));
            
            #line 30 "..\..\..\Control\CarModification.xaml"
            this.SaveEditionCar.Click += new System.Windows.RoutedEventHandler(this.SaveModificationOfCar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

