﻿using Combustion_Calculator.View;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for TankModificationControl.xaml
    /// </summary>
    public partial class TankModificationControl : UserControl
    {
        private DB db = new DB();
        int idUser;
        MainWindow main;
        string name;
        List<Tank> tankList;
        List<Currency> currencyList;
        
        
       
        public TankModificationControl(int id, MainWindow mainWindow, string nam)
        {
            idUser = id;
            main = mainWindow;
            name = nam;
            InitializeComponent();
            AddDetalisOfTankToCombo();
            AddCurrencyToCombo();
            errorMessage.Foreground = Brushes.Red;
        }
        public void AddDetalisOfTankToCombo()
        {
            
            var userCar = db.UserCar.Where(k => k.Name==name).FirstOrDefault();
            tankList = db.Tank.Where(k=> k.UserCarId== userCar.UserCarId).ToList();
            foreach (var x in tankList)
            {
                 SelectTank.Items.Add(x.AddDate);
            }
        }
        public void AddCurrencyToCombo()
        {
            currencyList = db.Currency.ToList();
            foreach (var x in currencyList)
            {
                SelectCurrency.Items.Add(x.Name);
            }
        }

        private void EditionTank_Click(object sender, RoutedEventArgs e)
        {
            if (SelectTank.SelectedItem != null && SelectCurrency.SelectedItem !=null)
            {

                foreach (var i in tankList)
                {
                    if (i.AddDate == (DateTime)SelectTank.SelectedItem)
                    {
                        //var tankChose = db.Tank.Where(k => k.AddDate == (DateTime)SelectTank.SelectedItem).FirstOrDefault();
                        i.NumberOfKm = Decimal.Parse(numberKilometers_Text.Text);
                        i.PrizeForLiter = Decimal.Parse(prize_Text.Text);
                        i.QuantityOfFuel = Decimal.Parse(quantity_Text.Text);
                        i.Currency = currencyList[SelectCurrency.SelectedIndex];
                        db.Tank.AddOrUpdate(i);
                        db.SaveChanges();
                        MessageBox.Show("Zmiany zostały zapisane");
                        numberKilometers_Text.Text = "";
                        prize_Text.Text = "";
                        quantity_Text.Text = "";
                        SelectCurrency.Text = "Wybierz walutę";
                        SelectTank.Text = "Wybierz datę tankowania";
                        break;
                    }
                }
            }
        }

        private void SelectTank_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectTank.SelectedItem != null)
            {
                foreach (var i in tankList)
                {

                    if (i.AddDate == (DateTime)SelectTank.SelectedItem)
                    {
                        numberKilometers_Text.Text = i.NumberOfKm.ToString();
                        prize_Text.Text = i.PrizeForLiter.ToString();
                        quantity_Text.Text = i.QuantityOfFuel.ToString();
                        //SelectCurrency.Text = i.Currency.Name;
                        var indexTest = currencyList.FindIndex(k => k.Name == i.Currency.Name);
                        SelectCurrency.SelectedIndex = indexTest;
                        if (i.DidYouMissTank == "1")
                        {
                            Yes_CheckBox.IsChecked = true;
                        }
                        else 
                        {
                            Yes_CheckBox.IsChecked = false;
                        }
                        break;
                    }
                }
            }
            else
            {
                errorMessage.Content = "Wybierz datę tankowania";
            }
            
        }
    }
}
