﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Combustion_Calculator.View;
using CombustionCalculator.DataAccessLayer.Model;
using CombustionCalculator.DataAccessLayer.DAL;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;

namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for AdminSettingsControl.xaml
    /// </summary>
    public partial class AdminSettingsControl : UserControl
    {
        int id;
        MainWindow win;
        private DB db = new DB();
        List<User> userList;
        

        public AdminSettingsControl(int iduser, MainWindow window)
        {
            id = iduser;
            win = window;
            InitializeComponent();
            AddToCombo(iduser);
            
        }
               
        private void GoToSettings_Click(object sender, RoutedEventArgs e)
        {
            win.ContentHolder.Content = new SettingsControl(id, win);

        }
        public void AddToCombo(int iduser)
        {
            userList = db.User.ToList();
            foreach (var x in userList)
            {
                if (x.UserId== iduser)
                    continue;
                else
                {
                    SelectUsers.Items.Add(x.Login);
                }
  
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
           
            if ((string)SelectUsers.SelectedItem != null)
            {
                foreach (var i in userList)
                {

                    if (i.Login == (string)SelectUsers.SelectedItem)
                    {
                        userList.Remove(i);
                        SelectUsers.Items.Remove(i.Login);
                        db.User.Remove(i);
                        db.SaveChanges();
                        MessageBox.Show("Użytkownik" + " " + i.Login + " " + "został usunięty");
                        SelectUsers.Text = "Wybierz użytkownika";
                        break;
                    } 
                }
            }
        }

        private void ChangePermissions_Click(object sender, RoutedEventArgs e)
        {
            if ((string)SelectUsers.SelectedItem != null)
            { 
                foreach (var i in userList)
                {
                    if (i.Login == (string)SelectUsers.SelectedItem)
                    {
                        var userRolesInDb = db.UserRoles.Where(k => k.UserId == i.UserId).FirstOrDefault();
                        if (userRolesInDb.RolesId == 1)
                        {
                            userRolesInDb.RolesId = 2;

                        }
                        else
                        {
                            userRolesInDb.RolesId = 1;
                        }
                        db.UserRoles.AddOrUpdate(userRolesInDb);
                        db.SaveChanges();
                        MessageBox.Show("Uprawnienia zostały zmienione");
                        break;
                    }
                }
            }
        }

        private void ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            foreach (var i in userList)
            {
                if ((string)SelectUsers.SelectedItem != null)
                {
                    if (i.Login == (string)SelectUsers.SelectedItem)
                    {
                        
                        win.ContentHolder.Content = new ChangePasswordAdminControl(id, win, i.Login);
                        

                    }
                    
                }
               
            }

            
        }

        private void SelectUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
