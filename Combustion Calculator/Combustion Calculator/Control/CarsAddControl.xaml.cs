﻿using Combustion_Calculator.View;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for CarsAdd.xaml
    /// </summary>
    public partial class CarsAddControl : UserControl
    {
        private DB db = new DB();
        int idUser;
        MainWindow main;
        List<Cars> carsList;
        
        public CarsAddControl(int id, MainWindow mainWindow)
        {
            idUser = id;
            main = mainWindow;
            InitializeComponent();
            AddToListView();
            ErrorMessage.Foreground = Brushes.Red;

        }
        public void AddToListView()
        {
            carsList = db.Cars.ToList();
            foreach (var x in carsList)
            {
                //carsList.Sort(x.Brand);
                this.Cars_ListView.Items.Add(x.Brand + " " + " " + x.Model);   
            }
        }

        private void AddCarToBase_Click(object sender, RoutedEventArgs e)
        {
            if (Cars_ListView.SelectedItem != null)
            {
                AddCars();
            }
            else
            {
                ErrorMessage.Content = "Zaznacz samochód";
            }

        }
        public void AddCars()
        {
            int num;
            if (Color_Text.Text.Length == 0 || NameOfCar_Text.Text.Length == 0 || NumberOfKm_Text.Text.Length == 0 || YearOfCar_Text.Text.Length == 0)
            {
                ErrorMessage.Content = "Wpisz dane";
            }

            else if (!Int32.TryParse(NumberOfKm_Text.Text, out num))
            {
                ErrorMessage.Content = "Wpisz liczbę";
                NumberOfKm_Text.Focus();
            }
            else if (!Int32.TryParse(YearOfCar_Text.Text, out num))
            {
                ErrorMessage.Content = "Wpisz liczbę";
                YearOfCar_Text.Focus();
            }
            else
            {
                UserCar userCar = new UserCar { CarsId = carsList[Cars_ListView.SelectedIndex].CarsId, Color = Color_Text.Text, Name = NameOfCar_Text.Text, NumberOfKm = Decimal.Parse(NumberOfKm_Text.Text), YearOfCar = Int32.Parse(YearOfCar_Text.Text), AddDate = DateTime.Now, ModificationDate = DateTime.Now, UserId = idUser };
                db.UserCar.Add(userCar);
                db.SaveChanges();
                MessageBox.Show("Dodano do bazy");
                main.ContentHolder.Content = new MainControl();
            }
        }

    }
}
