﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Data;
using CombustionCalculator.DataAccessLayer.Model;
using System.Data.Entity.Migrations;
using Combustion_Calculator.View;
using Combustion_Calculator.Helpers;

namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        int id;
        MainWindow win;
        
        private DB db = new DB();
        public SettingsControl(int iduser, MainWindow window)
        {
            id = iduser;
            win = window;
            
            InitializeComponent();
            ErrorMessage.Foreground = Brushes.Red;
        }

        private void Password_Click(object sender, RoutedEventArgs e)
        {
            if(SettingsHelper.CheckPassword(PasswordOld.Password, db, id))
            {
               if(!SettingsHelper.CheckIfPasswordTheSame(PasswordNew.Password, PasswordNewTwice.Password))
               {
                    ErrorMessage.Content = "Hasła muszą być takie same";
                    PasswordNew.Clear();
                    PasswordNewTwice.Clear();
                    PasswordNew.Focus();
               }
               if(PasswordNew.Password.Length == 0 || PasswordOld.Password.Length == 0 || PasswordNewTwice.Password.Length == 0)
               {
                    ErrorMessage.Content = "Wpisz dane";
               }
               else
               {
                    ErrorMessage.Content = "";
                    SettingsHelper.ChangePassword(PasswordOld.Password, PasswordNew.Password, db, id);
                    MessageBox.Show("Zmiany zostały zapisane");
                    win.ContentHolder.Content = new MainControl();
                }
            }
            else
            {
                ErrorMessage.Content = "Niepoprawne hasło dla tego użytkownika";
            }
        }
        
        public void CheckLanguage()
        {

        }

        private void Language_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Nie zaimplementowano");
        }
    }
}
