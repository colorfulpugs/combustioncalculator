﻿using Combustion_Calculator.View;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for CarModification.xaml
    /// </summary>
    public partial class CarModificationControl : UserControl
    {
        private DB db = new DB();
        int idUser;
        MainWindow main;
        List<UserCar> carsUserList;
        
        

        public CarModificationControl(int id, MainWindow mainWindow)
        {
            idUser = id;
            main = mainWindow;
            InitializeComponent();
            AddToBaseJustLogined(id);
        }
        public void AddToBaseJustLogined(int idU)
        {
          
            carsUserList = db.UserCar.Where(k => k.UserId == idU).ToList();
            foreach (var x in carsUserList)
            {
                var carOfUser = db.Cars.Where(k => k.CarsId == x.CarsId).FirstOrDefault();
                this.Cars_ListView.Items.Add(carOfUser.Brand + " " + " " + carOfUser.Model + " "  + " " + "'"+ x.Name + "'");
            }

        }

        private void Edition_Click(object sender, SelectionChangedEventArgs e)
        {
            if ((string)Cars_ListView.SelectedItem != null)
            {
                foreach (var i in carsUserList)
                {

                    if(i.UserCarId == carsUserList[Cars_ListView.SelectedIndex].UserCarId)
                    {

                        Color_Text.Text = i.Color;
                        YearOfCar_Text.Text =  i.YearOfCar.ToString();
                        NumberOfKm_Text.Text = i.NumberOfKm.ToString();
                        break;
                    }
                }
                
            }
        }

        private void SaveModificationOfCar_Click(object sender, RoutedEventArgs e)
        {
            if ((string)Cars_ListView.SelectedItem != null)
            {
                int indexCarList = carsUserList[Cars_ListView.SelectedIndex].UserCarId;
                var choosenCar = db.UserCar.Where(k => k.UserCarId == indexCarList).FirstOrDefault();

               choosenCar.Color = Color_Text.Text;
               choosenCar.NumberOfKm = Decimal.Parse(NumberOfKm_Text.Text);
               choosenCar.YearOfCar = Convert.ToInt32(YearOfCar_Text.Text);
               db.UserCar.AddOrUpdate(choosenCar);
               db.SaveChanges();
               MessageBox.Show("Zmiany zostały zapisane");
            }
        }

        private void DeleteCar_Click(object sender, RoutedEventArgs e)
        {
            if ((string)Cars_ListView.SelectedItem != null)
            {
                int indexCarList = carsUserList[Cars_ListView.SelectedIndex].UserCarId;
                var choosenCar = db.UserCar.Where(k => k.UserCarId == indexCarList).FirstOrDefault();
                carsUserList.Remove(choosenCar);
                Cars_ListView.Items.Remove(Cars_ListView.SelectedItem);
                Color_Text.Text = "";
                NumberOfKm_Text.Text = "";
                YearOfCar_Text.Text = "";
                db.UserCar.Remove(choosenCar);
                db.SaveChanges();
                MessageBox.Show("Samochód został usunięty");
            }
        }
    }
}
