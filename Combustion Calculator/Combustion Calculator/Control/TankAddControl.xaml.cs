﻿using Combustion_Calculator.View;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for TankControlAdd.xaml
    /// </summary>
    public partial class TankAddControl : UserControl
    {
        private DB db = new DB();
        int idUser;
        MainWindow main;
        string name;
        List<UserCar> carsList;
        List<Currency> currencyList;

        public TankAddControl(int id, MainWindow mainWindow, string nam)
        {
            idUser = id;
            main = mainWindow;
            name = nam;
            InitializeComponent();
            AddCurrencyToCombo();
            errorMessage.Foreground = Brushes.Red;
        }
        public void AddCurrencyToCombo()
        {
            currencyList = db.Currency.ToList();
            foreach (var x in currencyList)
            {
                SelectCurrency.Items.Add(x.Name);
            }
        }

        public void AddTank()
        {
            decimal num;
            if (numberKilometers_Text.Text.Length == 0 || prize_Text.Text.Length == 0 || quantity_Text.Text.Length == 0)
            {
                errorMessage.Content = "Wpisz dane";
            }

            else if (!Decimal.TryParse(numberKilometers_Text.Text, out num))
            {
                errorMessage.Content = "Wpisz liczbę";
                numberKilometers_Text.Focus();
            }
            else if (!Decimal.TryParse(prize_Text.Text, out num))
            {
                errorMessage.Content = "Wpisz liczbę";
                prize_Text.Focus();
            }
            else if (!Decimal.TryParse(quantity_Text.Text, out num))
            {
                errorMessage.Content = "Wpisz liczbę";
                quantity_Text.Focus();
            }
            else
            {
                if (SelectCurrency.SelectedItem != null)
                {
                    
                    carsList = db.UserCar.Where(k => k.UserId == idUser).ToList();
                    foreach (var x in carsList)
                    {

                        if (x.Name == name)
                        {
                            string carMiss;
                            if(Yes_CheckBox.IsChecked.Value)
                            {
                                carMiss = "1";
                            }
                            else
                            {
                                carMiss = "0";
                            }
                            
                            Tank tankCar = new Tank { NumberOfKm = Decimal.Parse(numberKilometers_Text.Text), PrizeForLiter = Decimal.Parse(prize_Text.Text), QuantityOfFuel = Decimal.Parse(quantity_Text.Text), CurrencyId = currencyList[SelectCurrency.SelectedIndex].CurrencyId, AddDate = DateTime.Now, UserCarId = x.UserCarId, DidYouMissTank = carMiss };
                            db.Tank.Add(tankCar);
                            db.SaveChanges();
                            MessageBox.Show("Dodano do bazy");
                            main.ContentHolder.Content = new MainControl();
                            break;

                        }
                    }
                }
                else
                {
                    errorMessage.Content = "Wybierz walutę";
                }

            }
        }

        private void SaveTank_Click(object sender, RoutedEventArgs e)
        {

            AddTank();
        }

       
    }
}
