﻿using Combustion_Calculator.View;
using CombustionCalculator.DataAccessLayer.DAL;
using CombustionCalculator.DataAccessLayer.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combustion_Calculator.Control
{
    /// <summary>
    /// Interaction logic for ChangePasswordAdminControl.xaml
    /// </summary>
    public partial class ChangePasswordAdminControl : UserControl
    {
        int id;
        MainWindow win;
        private DB db = new DB();
        string log;
        
        
        public ChangePasswordAdminControl(int idUser, MainWindow window, string login)
        {
            log = login;
            win = window;
            id = idUser;
            InitializeComponent();
            Username_Text.Text = login;
            ErrorMessage.Foreground = Brushes.Red;
        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {
             
            var user = db.User.Where(k => k.Login == log).FirstOrDefault();
          
            user.Login = Username_Text.Text;
            user.Password = PasswordName_Text.Password;
            if (Username_Text.Text.Length == 0 || PasswordName_Text.Password.Length == 0)
            {
                ErrorMessage.Content = "Wpisz dane";
                Username_Text.Focus();

            }
            else
            {
                ErrorMessage.Content = "";
                db.User.AddOrUpdate(user);
                db.SaveChanges();
                win.ContentHolder.Content = new AdminSettingsControl(id, win);
            }

        }
    }
}
