﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CombustionCalculator.RateExchange
{
    public class XmlParser
    {
        public static List<Rate> GetCurrencyListFromWeb(string xmlURL)
        {
            List<Rate> returnList = new List<Rate>();
            string date = string.Empty;
            WebClient client = new WebClient();
            client.Headers.Add("accept", "application/xml");
            string xmlInString = client.DownloadString(xmlURL);
            using (XmlReader xmlr = XmlReader.Create(new System.IO.StringReader(xmlInString)))
            {
                while (xmlr.ReadToFollowing("Rate"))
                {
                    XmlReader inner = xmlr.ReadSubtree();
                    string currency = "";
                    string code = "";
                    decimal mid = 0;
                    while (inner.Read())
                    {
                        if (xmlr.NodeType != XmlNodeType.Element)
                            continue;
                        if (inner.Name == "Currency")
                        {
                            inner.Read();
                            currency = inner.Value;
                        }
                        else if (inner.Name == "Code")
                        {
                            inner.Read();
                            code = inner.Value;
                        }
                        else if (inner.Name == "Mid")
                        {
                            inner.Read();
                            mid = decimal.Parse(inner.Value);
                        }
                    }
                    returnList.Add(new Rate(currency, code, mid));
                    inner.Close();
                }
            }
            return returnList;

        }
    
    }
}
