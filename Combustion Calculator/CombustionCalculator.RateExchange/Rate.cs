﻿namespace CombustionCalculator.RateExchange
{
    public class Rate
    {
        public decimal exchangeRate;
        public string name;
        public string code;

        public Rate(string nam, string cod, decimal exchange)
        {
            name = nam;
            code = cod;
            exchangeRate = exchange;
        }
       
    }
}